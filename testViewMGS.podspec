#
# Be sure to run `pod lib lint testView.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'testViewMGS'
  s.version          = '0.3.8'
  s.summary          = 'Integration of our SDK testView.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
this is a test for MGS integretion with views
                       DESC

  s.homepage         = 'https://GuillaumeMartinez@bitbucket.org/devmobile/testview'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'g.martinez@highconnexion.com' => 'g.martinez@highconnexion.com' }
  s.source           = { :git => 'https://GuillaumeMartinez@bitbucket.org/devmobile/testview.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'

  s.ios.vendored_frameworks = '*.framework'
  s.dependency 'com.hcnx.hcnx_base'

  s.resources = 'views*/*'
  s.resource_bundles = {
    'testView' => ['views/*']
   }


end
