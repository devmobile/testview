//
//  MGSViewController.h
//  testMGSViews
//
//  Created by Guillaume MARTINEZ on 24/01/2018.
//  Copyright © 2018 Guillaume MARTINEZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MGSViewControllerDelegate;

@interface MGSViewController : UINavigationController

@property (nonatomic, assign) id <MGSViewControllerDelegate> mDelegate;

- (instancetype)initWithType:(NSInteger)pType;

@end



@protocol MGSViewControllerDelegate

- (void)dismiss:(MGSViewController*)controller;

@end
