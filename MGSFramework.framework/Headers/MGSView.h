//
//  MGSView.h
//  MGSFramework
//
//  Created by Guillaume MARTINEZ on 18/01/2018.
//  Copyright © 2018 HighConnexion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HCNXBase/HCNXBase.h>

@interface MGSView : UIView

- (void)configureWithGame:(id)pMGSGame;
- (void)setTitleColor:(UIColor*)pColor;
- (void)setType:(NSInteger)pType;
- (void)prepareForReuse;
- (void)setBackgroundMGSViewColor:(UIColor*)pColor;
- (CGFloat)getHeight;

@end
