# testView

[![CI Status](http://img.shields.io/travis/g.martinez@highconnexion.com/testView.svg?style=flat)](https://travis-ci.org/g.martinez@highconnexion.com/testView)
[![Version](https://img.shields.io/cocoapods/v/testView.svg?style=flat)](http://cocoapods.org/pods/testView)
[![License](https://img.shields.io/cocoapods/l/testView.svg?style=flat)](http://cocoapods.org/pods/testView)
[![Platform](https://img.shields.io/cocoapods/p/testView.svg?style=flat)](http://cocoapods.org/pods/testView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

testView is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'testView'
```

## Author

g.martinez@highconnexion.com, g.martinez@highconnexion.com

## License

testView is available under the MIT license. See the LICENSE file for more info.
